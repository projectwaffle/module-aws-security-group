output "security_group_id" {
  description = "The ID of the security group"
  value       = "${element(concat(aws_security_group.main.*.id, list("")), 0)}"
}

output "security_group_name" {
  description = "The Name of the security group"
  value       = "${element(concat(aws_security_group.main.*.name, list("")), 0)}"
}
