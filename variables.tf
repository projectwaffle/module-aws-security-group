## Global variables

variable "resource_identities" {
  description = "Resource Identity according to PW Arhitecture Name Convention"
  type        = "map"
}

variable "global_tags" {
  description = "Global Tags"
  type        = "map"
}

## Module Specific variables

variable "module_tags" {
  description = "Module Tags"
  default     = {}
}

variable "name" {
  description = "Name for all resources to start with"
}

variable "vpc_id" {
  description = "ID of the VPC where to create security group"
}

## Resource variables

# Security Group

variable "create_security_group" {
  description = "Set to false if you do not want to create Security Group for module"
  default     = "true"
}

variable "security_group_tags" {
  description = "Custome Security Group Tags"
  default     = {}
}

# ingress Rules

variable "ingress_with_source_security_group_id" {
  description = "List of ingress rules to create where 'source_security_group_id' is used"
  default     = []
}

variable "ingress_with_cidr_blocks" {
  description = "List of ingress rules to create where 'cidr_blocks' is used"
  default     = []
}

variable "ingress_cidr_blocks" {
  description = "List of IPv4 CIDR ranges to use on all ingress rules"
  default     = []
}

variable "ingress_with_self" {
  description = "List of ingress rules to create where 'self' is defined"
  default     = []
}

# Egress variables

variable "egress_with_source_security_group_id" {
  description = "List of egress rules to create where 'source_security_group_id' is used"
  default     = []
}

variable "egress_with_cidr_blocks" {
  description = "List of egress rules to create where 'cidr_blocks' is used"
  default     = []
}

variable "egress_cidr_blocks" {
  description = "List of IPv4 CIDR ranges to use on all egress rules"
  default     = ["0.0.0.0/0"]
}

variable "egress_with_self" {
  description = "List of egress rules to create where 'self' is defined"
  default     = []
}

variable "egress_prefix_list_ids" {
  description = "List of egress rules to create where 'self' is defined"
  default     = []
}
