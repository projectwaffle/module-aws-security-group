# Locals

## Resources 

# Security Group Name, ingress and egress rules later

resource "aws_security_group" "main" {
  count = "${var.create_security_group ? 1 : 0}"

  name        = "${format("%s-%s", var.name, var.resource_identities["security_group"])}"
  description = "${format("%s-%s", var.name, var.resource_identities["security_group"])}"
  vpc_id      = "${var.vpc_id}"

  tags = "${merge(map("Name", format("%s-%s", var.name, var.resource_identities["security_group"],)), var.global_tags, var.module_tags, var.security_group_tags)}"
}

# Ingress Rules

# Security group rules with "source_security_group_id", but without "cidr_blocks" and "self"
resource "aws_security_group_rule" "ingress_with_source_security_group_id" {
  count = "${var.create_security_group ? length(var.ingress_with_source_security_group_id) : 0}"

  security_group_id = "${aws_security_group.main.id}"
  type              = "ingress"

  source_security_group_id = "${lookup(var.ingress_with_source_security_group_id[count.index], "source_security_group_id")}"
  description              = "${lookup(var.ingress_with_source_security_group_id[count.index], "description", "Ingress Rule")}"

  from_port = "${lookup(var.ingress_with_source_security_group_id[count.index], "from_port")}"
  to_port   = "${lookup(var.ingress_with_source_security_group_id[count.index], "to_port")}"
  protocol  = "${lookup(var.ingress_with_source_security_group_id[count.index], "protocol")}"
}

# Security group rules with "cidr_blocks", but without "source_security_group_id" and "self"
resource "aws_security_group_rule" "ingress_with_cidr_blocks" {
  count = "${var.create_security_group ? length(var.ingress_with_cidr_blocks) : 0}"

  security_group_id = "${aws_security_group.main.id}"
  type              = "ingress"

  cidr_blocks = ["${split(",", lookup(var.ingress_with_cidr_blocks[count.index], "cidr_blocks", join(",", var.ingress_cidr_blocks)))}"]
  description = "${lookup(var.ingress_with_cidr_blocks[count.index], "description", "Ingress Rule")}"

  from_port = "${lookup(var.ingress_with_cidr_blocks[count.index], "from_port")}"
  to_port   = "${lookup(var.ingress_with_cidr_blocks[count.index], "to_port")}"
  protocol  = "${lookup(var.ingress_with_cidr_blocks[count.index], "protocol")}"
}

# Security group rules with "self", but without "cidr_blocks" and "source_security_group_id"
resource "aws_security_group_rule" "ingress_with_self" {
  count = "${var.create_security_group ? length(var.ingress_with_self) : 0}"

  security_group_id = "${aws_security_group.main.id}"
  type              = "ingress"

  self        = "${lookup(var.ingress_with_self[count.index], "self", true)}"
  description = "${lookup(var.ingress_with_self[count.index], "description", "Ingress Rule")}"

  from_port = "${lookup(var.ingress_with_self[count.index], "from_port")}"
  to_port   = "${lookup(var.ingress_with_self[count.index], "to_port")}"
  protocol  = "${lookup(var.ingress_with_self[count.index], "protocol")}"
}

# Egress Rules

# Security group rules with "source_security_group_id", but without "cidr_blocks" and "self"
resource "aws_security_group_rule" "egress_with_source_security_group_id" {
  count = "${var.create_security_group ? length(var.egress_with_source_security_group_id) : 0}"

  security_group_id = "${aws_security_group.main.id}"
  type              = "egress"

  source_security_group_id = "${lookup(var.egress_with_source_security_group_id[count.index], "source_security_group_id")}"
  prefix_list_ids          = ["${var.egress_prefix_list_ids}"]
  description              = "${lookup(var.egress_with_source_security_group_id[count.index], "description", "Egress Rule")}"

  from_port = "${lookup(var.egress_with_source_security_group_id[count.index], "from_port")}"
  to_port   = "${lookup(var.egress_with_source_security_group_id[count.index], "to_port")}"
  protocol  = "${lookup(var.egress_with_source_security_group_id[count.index], "protocol")}"
}

# Security group rules with "cidr_blocks", but without "ipv6_cidr_blocks", "source_security_group_id" and "self"
resource "aws_security_group_rule" "egress_with_cidr_blocks" {
  count = "${var.create_security_group ? length(var.egress_with_cidr_blocks) : 0}"

  security_group_id = "${aws_security_group.main.id}"
  type              = "egress"

  cidr_blocks     = ["${split(",", lookup(var.egress_with_cidr_blocks[count.index], "cidr_blocks", join(",", var.egress_cidr_blocks)))}"]
  prefix_list_ids = ["${var.egress_prefix_list_ids}"]
  description     = "${lookup(var.egress_with_cidr_blocks[count.index], "description", "Egress Rule")}"

  from_port = "${lookup(var.egress_with_cidr_blocks[count.index], "from_port")}"
  to_port   = "${lookup(var.egress_with_cidr_blocks[count.index], "to_port")}"
  protocol  = "${lookup(var.egress_with_cidr_blocks[count.index], "protocol")}"
}

# Security group rules with "self", but without "cidr_blocks" and "source_security_group_id"
resource "aws_security_group_rule" "egress_with_self" {
  count = "${var.create_security_group ? length(var.egress_with_self) : 0}"

  security_group_id = "${aws_security_group.main.id}"
  type              = "egress"

  self            = "${lookup(var.egress_with_self[count.index], "self", true)}"
  prefix_list_ids = ["${var.egress_prefix_list_ids}"]
  description     = "${lookup(var.egress_with_self[count.index], "description", "Egress Rule")}"

  from_port = "${lookup(var.egress_with_self[count.index], "from_port")}"
  to_port   = "${lookup(var.egress_with_self[count.index], "to_port")}"
  protocol  = "${lookup(var.egress_with_self[count.index], "protocol")}"
}
