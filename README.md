# AWS Security Group Terraform module

[![CircleCI](https://circleci.com/bb/projectwaffle/module-aws-security-group.svg?style=svg)](https://circleci.com/bb/projectwaffle/module-aws-security-group/)

Terraform module which creates Security Group on AWS.

These types of resources are supported:

* [Security Group](https://www.terraform.io/docs/providers/aws/r/security_group.html)
* [Security Group Rule](https://www.terraform.io/docs/providers/aws/r/security_group_rule.html)

Sponsored by [Draw.io - the best way to draw AWS diagrams](https://www.draw.io/)

## Usage

```hcl
module "sg_web" {
  source  = "bitbucket.org/projectwaffle/module-aws-security-group.git?ref=tags/v0.0.1"

  resource_identities = {
    security_group = "sg"
  }

  global_tags     = "${var.global_tags}"
  create_security_group = "true"
  vpc_id = "${module.vpc.vpc_id}"

  ingress_with_cidr_blocks = [
    {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      description = "Allow Port 80"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      description = "Allow Port 443"
      cidr_blocks = "0.0.0.0/0"
    }
  ],
  egress_with_cidr_blocks = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      description = "Allow Any"
      cidr_blocks = "0.0.0.0/0"
    }
  ]
}
```

## Terraform version

Terraform version 0.11.10 or newer is required for this module to work.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| create\_security\_group | Set to false if you do not want to create Security Group for module | string | `true` | no |
| egress\_cidr\_blocks | List of IPv4 CIDR ranges to use on all egress rules | list | `<list>` | no |
| egress\_prefix\_list\_ids | List of egress rules to create where 'self' is defined | list | `<list>` | no |
| egress\_with\_cidr\_blocks | List of egress rules to create where 'cidr_blocks' is used | list | `<list>` | no |
| egress\_with\_self | List of egress rules to create where 'self' is defined | list | `<list>` | no |
| egress\_with\_source\_security\_group\_id | List of egress rules to create where 'source_security_group_id' is used | list | `<list>` | no |
| global\_tags | Global Tags | map | - | yes |
| ingress\_cidr\_blocks | List of IPv4 CIDR ranges to use on all ingress rules | list | `<list>` | no |
| ingress\_with\_cidr\_blocks | List of ingress rules to create where 'cidr_blocks' is used | list | `<list>` | no |
| ingress\_with\_self | List of ingress rules to create where 'self' is defined | list | `<list>` | no |
| ingress\_with\_source\_security\_group\_id | List of ingress rules to create where 'source_security_group_id' is used | list | `<list>` | no |
| module\_tags | Module Tags | map | `<map>` | no |
| name | Name for all resources to start with | string | - | yes |
| resource\_identities | Resource Identity according to PW Arhitecture Name Convention | map | - | yes |
| security\_group\_tags | Custome Security Group Tags | map | `<map>` | no |
| vpc\_id | ID of the VPC where to create security group | string | - | yes |

## Outputs

| Name | Description |
|------|-------------|
| security\_group\_id | The ID of the security group |
| security\_group\_name | The Name of the security group |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->


## Authors

Module is maintained by [Sergiu Plotnicu](https://bitbucket.org/projectwaffle/)

## License

Licensed under Sergiu Plotnicu, please contact him at - sergiu.plotnicu@gmail.com


